package com.home.work.service;

public interface BasketService {

    void deleteBasketForUser(String username);
    
    void createBasketForUser(String username);
}
