package com.home.work.service.implementation;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.home.work.entity.ApplicationUser;
import com.home.work.repository.UserRepository;
import com.home.work.service.BasketService;
import com.home.work.service.exceptions.UserNotFoundException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class BasketServiceImpl implements BasketService {


    private final UserRepository userRepository;

    @Override
    public void deleteBasketForUser(String username) {
        ApplicationUser user = userRepository.findByUsername(username)
                .orElseThrow((UserNotFoundException::new));

        user.setLastConnectionDate(LocalDateTime.now());
        user.setBasket(null);
        userRepository.save(user);
    }

    @Override
    public void createBasketForUser(String username) {
        ApplicationUser user = userRepository.findByUsername(username)
                .orElseThrow((UserNotFoundException::new));


        user.setBasket(new ArrayList<>());
        userRepository.save(user);
    }  
    
}
