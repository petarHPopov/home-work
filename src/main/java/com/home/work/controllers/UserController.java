package com.home.work.controllers;

import com.home.work.entity.ApplicationUser;
import com.home.work.entity.Item;
import com.home.work.models.ItemDTO;
import com.home.work.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RequiredArgsConstructor
@RestController
@Slf4j
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @PutMapping("/items")
    public ResponseEntity<?> addItem(@RequestBody ItemDTO item, Principal principal) {

        return userService.addItem(item, principal);


    }

    @GetMapping("/items")
    public ResponseEntity<List<Item>> getAllItemForUser(@RequestParam Long userId) {

        return ResponseEntity.ok(userService.getAllItemForUser(userId));

    }

}
