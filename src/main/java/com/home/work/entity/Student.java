package com.home.work.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.*;

@Entity
@DiscriminatorValue("student")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Student extends User {

    private String studentId;

    private String grade;
}
