package com.home.work.service;

import com.home.work.entity.ApplicationUser;
import com.home.work.entity.Item;
import com.home.work.models.ActiveUserDTO;
import com.home.work.models.ItemDTO;
import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;


import java.security.Principal;
import java.util.List;

public interface UserService {
     List<ActiveUserDTO> getActiveUsers();

    List<ApplicationUser> getInfoAllUsers();

    ResponseEntity<?> addItem(ItemDTO item, Principal principal);

    List<Item> getAllItemForUser(Long userId);
}
