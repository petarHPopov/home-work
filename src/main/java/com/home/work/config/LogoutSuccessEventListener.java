package com.home.work.config;

import com.home.work.service.BasketService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.LogoutSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class LogoutSuccessEventListener implements ApplicationListener<LogoutSuccessEvent> {

    private final BasketService basketService;

    @Override
    public void onApplicationEvent(LogoutSuccessEvent event) {
        Authentication authentication = event.getAuthentication();
        if (authentication != null) {
            String username = authentication.getName();

            basketService.deleteBasketForUser(username);
        }
    }
}

